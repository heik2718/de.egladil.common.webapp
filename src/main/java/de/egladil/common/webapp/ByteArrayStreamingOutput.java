//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.IOUtils;

/**
 * ByteArrayStreamingOutput
 */
public class ByteArrayStreamingOutput implements StreamingOutput {

	private final byte[] daten;

	/**
	 * ByteArrayStreamingOutput
	 */
	public ByteArrayStreamingOutput(final byte[] daten) {
		this.daten = daten;
	}

	@Override
	public void write(final OutputStream output) throws IOException, WebApplicationException {
		try {
			output.write(daten);
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
		}
	}
}
