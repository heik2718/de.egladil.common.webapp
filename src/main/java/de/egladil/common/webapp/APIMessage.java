//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * APIMessage
 */
public class APIMessage {

	@JsonProperty
	private String level;

	@JsonProperty
	private String message;

	/**
	 * APIMessage
	 */
	APIMessage() {
	}

	public APIMessage(final String level, final String message) {
		this.level = level;
		this.message = message;
	}

	/**
	 *
	 * @return APIMessage mit level INFO
	 */
	public static APIMessage info(final String message) {
		final APIMessage result = new APIMessage();
		result.level = MessageLevel.INFO.toString();
		result.message = message;
		return result;
	}

	/**
	 *
	 * @return APIMessage mit level WARN
	 */
	public static APIMessage warn(final String message) {
		final APIMessage result = new APIMessage();
		result.level = MessageLevel.WARN.toString();
		result.message = message;
		return result;
	}

	/**
	 *
	 * @return APIMessage mit level ERROR
	 */
	public static APIMessage error(final String message) {
		final APIMessage result = new APIMessage();
		result.level = MessageLevel.ERROR.toString();
		result.message = message;
		return result;
	}

	public final String getLevel() {
		return level;
	}

	public final String getMessage() {
		return message;
	}
}
