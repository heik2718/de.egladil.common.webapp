//=====================================================
// Projekt: de.egladil.common.webapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import java.util.Optional;

import de.egladil.common.persistence.EgladilDuplicateEntryException;

/**
* UniqueIndexNameMessageProvider
*/
public interface UniqueIndexNameMessageProvider {

	Optional<String> exceptionToMessage(final EgladilDuplicateEntryException e);
}
