//=====================================================
// Projekt: de.egladil.common.webapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * MessageLevel
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MessageLevel {

	INFO,
	WARN,
	ERROR;

}
