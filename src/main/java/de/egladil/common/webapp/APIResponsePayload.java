//=====================================================
// Projekt: de.egladil.common.webapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * APIResponsePayload
 */
public class APIResponsePayload {

	@JsonProperty
	private APIMessage apiMessage;

	@JsonProperty
	private Object payload;

	@JsonProperty
	private SessionToken sessionToken;


	/**
	 * APIResponsePayload
	 */
	APIResponsePayload() {
	}

	/**
	 * APIResponsePayload
	 */
	public APIResponsePayload(final APIMessage apiMessage) {
		this.apiMessage = apiMessage;
	}

	/**
	 * APIResponsePayload
	 */
	public APIResponsePayload(final APIMessage apiMessage, final Object payload) {
		this.apiMessage = apiMessage;
		this.payload = payload;
	}

	public final APIMessage getApiMessage() {
		return apiMessage;
	}

	public final Object getPayload() {
		return payload;
	}

	public void setSessionToken(SessionToken sessionToken) {
		this.sessionToken = sessionToken;
	}
}
