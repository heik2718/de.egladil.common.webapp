//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.persistence.PersistenceException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.EgladilAuthorizationException;
import de.egladil.common.exception.InvalidInputException;
import de.egladil.common.exception.NotImplementedException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.exception.ResourceNotFoundException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.persistence.ILoggable;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.webapp.exception.CustomResponseStatus;
import de.egladil.common.webapp.exception.EgladilWebappException;
import de.egladil.common.webapp.exception.ExceptionFactory;
import de.egladil.common.webapp.exception.IAdaptedMessage;
import de.egladil.common.webapp.exception.MessageAdapter;

/**
 * ResourceExceptionHandler
 */
public class ResourceExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ResourceExceptionHandler.class);

	private static final ResourceBundle APP_MESSAGES = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	private final UniqueIndexNameMessageProvider uniqueIndexNameMessageProvider;

	/**
	 * Erzeugt eine Instanz von ResourceExceptionHandler
	 */
	public ResourceExceptionHandler(final UniqueIndexNameMessageProvider uniqueIndexNameMessageProvider) {
		this.uniqueIndexNameMessageProvider = uniqueIndexNameMessageProvider;
	}

	/**
	 * Mapped die gegebene Exception in ein Response mit payload vom Typ PublicMessage.<br>
	 * <br>
	 * Gemapped werden (in dieser Reihenfolge)
	 * <ul>
	 * <li>EgladilWebappException</li>
	 * <li>EgladilAuthenticationException</li>
	 * <li>DisabledAccountException</li>
	 * <li>EgladilAuthorizationException</li>
	 * <li>WebApplicationException)</li>
	 * <li>ResourceNotFoundException</li>
	 * <li>PreconditionFailedException</li>
	 * <li>EgladilConcurrentModificationException (902)</li>
	 * <li>EgladilDuplicateEntryException (900 oder 500) <strong>Wir als einzige gelogged</strong></li>
	 * <li>PersistenceException</li>
	 * <li>Exception</li>
	 * </ul>
	 *
	 * @param exception Throwable
	 * @param appMessageKey String das teil zu ApplicationMessages_de.property. Darf null sein.
	 * @param requestPayload ILoggable zum Loggen, wenn erforderlich.
	 * @return Response
	 */
	public Response mapToMKVApiResponse(final Throwable exception, final String appMessageKey, final ILoggable requestPayload) {
		return this.mapToMKVApiResponse(exception, appMessageKey, requestPayload, null);
	}

	/**
	 * Mapped die gegebene Exception in ein Response mit payload vom Typ PublicMessage.<br>
	 * <br>
	 * Gemapped werden (in dieser Reihenfolge)
	 * <ul>
	 * <li>EgladilWebappException</li>
	 * <li>EgladilAuthenticationException</li>
	 * <li>DisabledAccountException</li>
	 * <li>EgladilAuthorizationException</li>
	 * <li>WebApplicationException)</li>
	 * <li>ResourceNotFoundException</li>
	 * <li>PreconditionFailedException</li>
	 * <li>EgladilConcurrentModificationException (902)</li>
	 * <li>EgladilDuplicateEntryException (900 oder 500) <strong>Wir als einzige gelogged</strong></li>
	 * <li>PersistenceException</li>
	 * <li>Exception</li>
	 * </ul>
	 *
	 * @param exception Throwable
	 * @param appMessageKey String das teil zu ApplicationMessages_de.property. Darf null sein.
	 * @param requestPayload ILoggable zum Loggen, wenn erforderlich.
	 * @param concurrentlyModifiedPayload Object falls es zu einer ConcurrentModification kam, muss das das aktuelle
	 * Objekt sein, damit der Client nicht nochmal nachlesen muss. wenn null, wird's ignoriert.
	 * @return Response
	 */
	public Response mapToMKVApiResponse(final Throwable exception, final String appMessageKey, final ILoggable requestPayload,
		final Object concurrentlyModifiedPayload) {

		APIResponsePayload entity = null;

		if (exception instanceof EgladilWebappException) {
			final EgladilWebappException theException = (EgladilWebappException) exception;
			final IAdaptedMessage adaptedMessage = MessageAdapter.adapt(theException.getPayload());
			if (adaptedMessage != null) {
				final APIMessage apiMessage = new APIMessage(adaptedMessage.getMessageLevel(), adaptedMessage.getMessage());
				entity = new APIResponsePayload(apiMessage, adaptedMessage.getPayload());
			} else {
				entity = getMKVApiResponseServerError();
			}
			final Response wrapped = ((EgladilWebappException) exception).getResponse();
			try {
				System.out.println(new ObjectMapper().writeValueAsString(entity));
			} catch (final Exception e) {
				// nüscht
			}
			return Response.status(wrapped.getStatus()).entity(entity).build();
		}

		if (exception instanceof EgladilAuthenticationException) {
			final String msg = APP_MESSAGES.getString("general.notAuthenticated");
			entity = new APIResponsePayload(APIMessage.error(msg));
			return Response.status(CustomResponseStatus.UNAUTHORIZED.getStatusCode()).entity(entity).build();
		}

		if (exception instanceof DisabledAccountException) {
			final String msg = APP_MESSAGES.getString("authentication.disabledAccount");
			entity = new APIResponsePayload(APIMessage.warn(msg));
			return Response.status(Status.UNAUTHORIZED.getStatusCode()).entity(entity).build();
		}

		if (exception instanceof EgladilAuthorizationException) {
			final String msg = APP_MESSAGES.getString("general.notAuthorized");
			entity = new APIResponsePayload(APIMessage.error(msg));
			return Response.status(CustomResponseStatus.UNAUTHORIZED.getStatusCode()).entity(entity).build();
		}
		if (exception instanceof NotImplementedException) {
			entity = new APIResponsePayload(APIMessage.error(exception.getMessage()));
			return Response.status(CustomResponseStatus.TEAPOT.getStatusCode()).entity(entity).build();
		}

		if (exception instanceof InvalidInputException) {
			entity = new APIResponsePayload(APIMessage.error(exception.getMessage()));
			return Response.status(CustomResponseStatus.VALIDATION_ERRORS.getStatusCode()).entity(entity).build();
		}

		if (exception instanceof WebApplicationException) {
			final WebApplicationException theException = (WebApplicationException) exception;
			final String msg = theException.getMessage();
			final int status = theException.getResponse().getStatus();
			entity = new APIResponsePayload(APIMessage.error(msg));
			return Response.status(status).entity(entity).build();
		}

		if (exception instanceof ResourceNotFoundException) {
			final ResourceNotFoundException theException = (ResourceNotFoundException) exception;
			final String msg = theException.getMessage();
			entity = new APIResponsePayload(APIMessage.error(msg));
			return Response.status(Status.NOT_FOUND.getStatusCode()).entity(entity).build();
		}

		if (exception instanceof PreconditionFailedException) {
			final String msgKey = appMessageKey != null ? appMessageKey : "genaral.precondition.failed";
			entity = new APIResponsePayload(APIMessage.error(APP_MESSAGES.getString(msgKey)));
			return Response.status(Status.PRECONDITION_FAILED.getStatusCode()).entity(entity).build();
		}

		if (exception instanceof EgladilConcurrentModificationException) {
			return createConcurrentModificationResponse(appMessageKey, requestPayload, concurrentlyModifiedPayload);
		}

		if (exception instanceof EgladilDuplicateEntryException) {
			return createDuplicateEntryResponse(exception, appMessageKey, requestPayload);
		}

		if (exception instanceof PersistenceException) {
			final PersistenceException theException = (PersistenceException) exception;
			final Optional<EgladilDuplicateEntryException> optDuplicate = PersistenceExceptionMapper
				.toEgladilDuplicateEntryException(theException);
			if (optDuplicate.isPresent()) {
				return createDuplicateEntryResponse(optDuplicate.get(), appMessageKey, requestPayload);
			}
			final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(theException);
			if (optConc.isPresent()) {
				return createConcurrentModificationResponse(appMessageKey, requestPayload, concurrentlyModifiedPayload);
			}
			entity = getMKVApiResponseServerError();
			return Response.serverError().entity(entity).build();
		}
		entity = getMKVApiResponseServerError();
		return Response.serverError().entity(entity).build();
	}

	private Response createDuplicateEntryResponse(final Throwable exception, final String appMessageKey, final ILoggable payload) {
		final EgladilDuplicateEntryException theException = (EgladilDuplicateEntryException) exception;
		if (appMessageKey == null) {
			final Optional<String> opt = uniqueIndexNameMessageProvider.exceptionToMessage(theException);
			if (opt.isPresent()) {
				final String msg = opt.get();
				LOG.warn(msg);
				final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(msg));
				return Response.status(CustomResponseStatus.DUPLICATE_ENTRY.getStatusCode()).entity(entity).build();
			} else {
				LOG.error("Unbekannter UniqueIndexName [{}] mit payload {}", theException.getUniqueIndexName(), payload);
				throw ExceptionFactory.internalServerErrorException();
			}
		} else {
			final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(APP_MESSAGES.getString(appMessageKey)));
			return Response.status(CustomResponseStatus.DUPLICATE_ENTRY.getStatusCode()).entity(entity).build();
		}
	}

	private Response createConcurrentModificationResponse(final String appMessageKey, final ILoggable payload,
		final Object concurrentlyModifiedPayload) {

		final String msg = appMessageKey != null ? APP_MESSAGES.getString(appMessageKey)
			: APP_MESSAGES.getString("general.concurrent");
		final APIResponsePayload entity = new APIResponsePayload(APIMessage.warn(msg), concurrentlyModifiedPayload);
		return Response.status(CustomResponseStatus.CONCURRENT_MODIFICATION.getStatusCode()).entity(entity).build();
	}

	private APIResponsePayload getMKVApiResponseServerError() {
		return new APIResponsePayload(APIMessage.error(APP_MESSAGES.getString("general.internalServerError")));
	}
}
