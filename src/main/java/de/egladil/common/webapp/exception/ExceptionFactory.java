//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;

/**
 * ExceptionFactory bastelt custom WebApplicationExceptions.
 */
public final class ExceptionFactory {

	/**
	 *
	 */
	public ExceptionFactory() {
		super();
	}

	private static final Logger LOG = LoggerFactory.getLogger(ExceptionFactory.class);

	private static final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	// Katalogantrag.error.mailserver

	public static EgladilWebappException internalServerErrorException() {
		final String message = applicationMessages.getString("general.internalServerError");
		return new EgladilWebappException(CustomResponseStatus.SERVER_ERROR, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException internalServerErrorException(final String message) {
		return new EgladilWebappException(CustomResponseStatus.SERVER_ERROR, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException internalServerErrorException(final Logger logger, @Nullable final Throwable throwable,
		@Nullable final String details) {
		final String message = applicationMessages.getString("general.internalServerError");
		LogUtils.logError(logger == null ? LOG : logger, CustomResponseStatus.SERVER_ERROR, message, details, throwable);
		return new EgladilWebappException(CustomResponseStatus.SERVER_ERROR, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException resourceNotFound(final String text) {
		return new EgladilWebappException(CustomResponseStatus.NOT_FOUND, new APIResponsePayload(APIMessage.error(text)));
	}

	public static EgladilWebappException resourceNotFound(final Logger logger, @Nullable final String details) {
		final String message = applicationMessages.getString("general.notFound");
		LogUtils.logError(logger == null ? LOG : logger, CustomResponseStatus.NOT_FOUND, message, details, null);
		return new EgladilWebappException(CustomResponseStatus.NOT_FOUND, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException unprocessableEntity() {
		final String message = applicationMessages.getString("general.unprocessableEntity");
		return new EgladilWebappException(CustomResponseStatus.UNPROCESSABLE_ENTITY,
			new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException badRequest() {
		final String message = applicationMessages.getString("general.badRequest");
		return badRequest(message);
	}

	public static EgladilWebappException forbidden() {
		final String message = applicationMessages.getString("general.forbidden");
		return new EgladilWebappException(CustomResponseStatus.FORBIDDEN, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException forbidden(final String text) {
		return new EgladilWebappException(CustomResponseStatus.FORBIDDEN, text);
	}

	public static EgladilWebappException notAuthenticated(final Logger logger, @Nullable final Throwable throwable,
		@Nullable final String details) {
		final String message = applicationMessages.getString("general.notAuthenticated");
		LogUtils.logError(logger == null ? LOG : logger, CustomResponseStatus.UNAUTHORIZED, message, details, throwable);
		return new EgladilWebappException(CustomResponseStatus.UNAUTHORIZED, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException notAuthenticated(final Logger logger, final String message,
		@Nullable final Throwable throwable, @Nullable final String details) {
		LogUtils.logError(logger == null ? LOG : logger, CustomResponseStatus.UNAUTHORIZED, message, details, throwable);
		return new EgladilWebappException(CustomResponseStatus.UNAUTHORIZED, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException notAuthorized() {
		final String message = applicationMessages.getString("general.notAuthorized");
		return new EgladilWebappException(CustomResponseStatus.UNAUTHORIZED, new APIResponsePayload(APIMessage.error(message)));
	}

	/**
	 * Erzeugt mit der message ein 900-Response, sofern message vorhanden.<br>
	 * <br>
	 * Der Parameter uniqueIndexName wird für das Exception-Logging benötigt. Der Parameter message muss durch den
	 * Aufrufer gesetzt werden, am besten mittels des UniqueIndexMappers.<br>
	 * <br>
	 * Beispiel: Optional<String> message = UniqueIndexNameMapper.exceptionToMessage(e);
	 *
	 * @param uniqueIndexName String der Index-Name aus der EgladilDuplicateEntryException.
	 * @param message Optional - sollte vorhanden sein, damit kein internalServerError generiert wird.
	 * @return
	 */
	public static EgladilWebappException duplicateEntry(final String uniqueIndexName, final Optional<String> message) {
		if (message.isPresent()) {
			return new EgladilWebappException(CustomResponseStatus.DUPLICATE_ENTRY,
				new APIResponsePayload(APIMessage.warn(message.get())));
		}
		LOG.error("Unbekannter uniqueIndexName {}", uniqueIndexName);
		return internalServerErrorException();
	}

	public static EgladilWebappException badRequest(final String message) {
		return new EgladilWebappException(CustomResponseStatus.BAD_REQUEST, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException badRequest(final String message, final Logger logger, @Nullable final Throwable throwable,
		@Nullable final String details) {
		LogUtils.logError(logger == null ? LOG : logger, CustomResponseStatus.BAD_REQUEST, message, details, throwable);
		return new EgladilWebappException(CustomResponseStatus.BAD_REQUEST, new APIResponsePayload(APIMessage.error(message)));
	}

	public static EgladilWebappException validationErrors(final ConstraintViolationMessage entity) {
		return new EgladilWebappException(CustomResponseStatus.VALIDATION_ERRORS, entity);
	}

	public static EgladilWebappException concurrentModification(final String message) {
		return new EgladilWebappException(CustomResponseStatus.CONCURRENT_MODIFICATION,
			new APIResponsePayload(APIMessage.warn(message)));
	}

	public static EgladilWebappException teapot(final String message) {
		return new EgladilWebappException(CustomResponseStatus.TEAPOT, new APIResponsePayload(APIMessage.warn(message)));
	}
}
