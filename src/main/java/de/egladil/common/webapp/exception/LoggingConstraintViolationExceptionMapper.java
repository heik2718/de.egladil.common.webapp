//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;

import io.dropwizard.jersey.validation.ValidationErrorMessage;

/**
 * ConstraintViolationExceptionMapper
 */
@Provider
public class LoggingConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

	private static final Logger LOG = LoggerFactory.getLogger(LoggingConstraintViolationExceptionMapper.class);

	/**
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(ConstraintViolationException exception) {

		ImmutableList<String> errors = FluentIterable.from(exception.getConstraintViolations())
			.transform(new Function<ConstraintViolation<?>, String>() {
				@Override
				public String apply(ConstraintViolation<?> v) {
					String value = v.getInvalidValue() != null ? v.getInvalidValue().toString() : "";
					final String message = value + " " + v.getMessage();
					LogUtils.logValidationError(LOG, CustomResponseStatus.UNPROCESSABLE_ENTITY, message, value, exception);
					return message;
				}
			}).toList();

		if (errors.size() == 0) {
			errors = ImmutableList.of(Strings.nullToEmpty(exception.getMessage()));
		}

		return Response.status(CustomResponseStatus.UNPROCESSABLE_ENTITY.getStatusCode()).entity(new ValidationErrorMessage(errors)).build();
	}
}
