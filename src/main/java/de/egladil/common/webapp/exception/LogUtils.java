//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import java.text.MessageFormat;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LogUtils
 */
public final class LogUtils {

	private static final Logger LOG = LoggerFactory.getLogger(LogUtils.class);

	private static final String TWO_ARGS = "{0} - {1}";

	private static final String THREEE_ARGS = "{0} - {1} {2}";

	private static final String FOUR_ARGS = "{0} - {1} {2} {3}";

	/**
	 * Erzeugt eine Instanz von LogUtils
	 */
	private LogUtils() {
	}

	/**
	 * Erzeugt einen Eintrag im Log, falls das Loglevel des responseStatus nicht NULL ist.
	 *
	 * @param originalLogger
	 * @param responseStatus
	 * @param message
	 * @param details
	 * @param exception
	 */
	public static void logError(Logger originalLogger, CustomResponseStatus responseStatus, String message,
		@Nullable String details, @Nullable Throwable exception) {
		if (responseStatus.getLoglevel() == Loglevel.NULL) {
			return;
		}
		if (details == null) {
			logWithoutDetails(originalLogger == null ? LOG : originalLogger, responseStatus, message, exception);
		} else {
			logWithDetails(originalLogger == null ? LOG : originalLogger, responseStatus, message, details, exception);
		}
	}


	/**
	 * Erzeugt einen Eintrag im Log, falls das Loglevel des responseStatus nicht NULL ist.
	 *
	 * @param originalLogger
	 * @param responseStatus
	 * @param invalidPropertiy
	 * @param invalidValue
	 * @param exception
	 */
	public static void logValidationError(Logger originalLogger, CustomResponseStatus responseStatus, String invalidPropertiy,
		@Nullable String invalidValue, @Nullable Throwable exception) {
		if (responseStatus.getLoglevel() == Loglevel.NULL) {
			return;
		}
		if (invalidValue == null) {
			logWithoutDetails(originalLogger == null ? LOG : originalLogger, responseStatus, invalidPropertiy, exception);
		} else {
			logWithDetails(originalLogger == null ? LOG : originalLogger, responseStatus, invalidPropertiy, invalidValue, exception);
		}
	}

	/**
	 * TODO
	 *
	 * @param originalLogger
	 * @param responseStatus
	 * @param message
	 * @param details
	 * @param exception
	 */

	private static void logWithDetails(Logger originalLogger, CustomResponseStatus responseStatus, String message, String details,
		Throwable exception) {
		if (exception == null) {
			String logMessage = MessageFormat.format(THREEE_ARGS, responseStatus.name(), message, details);
			doLog(originalLogger, responseStatus, logMessage);
		} else {
			String logMessage = MessageFormat.format(FOUR_ARGS, responseStatus.name(), message, details, exception.getMessage());

			doLog(originalLogger, responseStatus, exception, logMessage);
		}
	}

	/**
	 * TODO
	 *
	 * @param originalLogger
	 * @param responseStatus
	 * @param message
	 * @param exception
	 */

	private static void logWithoutDetails(Logger originalLogger, CustomResponseStatus responseStatus, String message,
		Throwable exception) {
		if (exception == null) {
			String logMessage = MessageFormat.format(TWO_ARGS, responseStatus.name(), message);
			doLog(originalLogger, responseStatus, logMessage);
		} else {
			String logMessage = MessageFormat.format(THREEE_ARGS, responseStatus.name(), message, exception.getMessage());
			doLog(originalLogger, responseStatus, exception, logMessage);
		}
	}

	/**
	 * TODO
	 *
	 * @param originalLogger
	 * @param responseStatus
	 * @param exception
	 * @param logMessage
	 */

	private static void doLog(Logger originalLogger, CustomResponseStatus responseStatus, Throwable exception, String logMessage) {
		switch (responseStatus.getLoglevel()) {
		case DEBUG:
			originalLogger.debug(logMessage, exception);
			break;
		case INFO:
			originalLogger.info(logMessage);
			break;
		case WARN:
			originalLogger.warn(logMessage);
			break;
		case ERROR:
			originalLogger.error(logMessage, exception);
			break;
		default:
			break;
		}
	}

	/**
	 * TODO
	 *
	 * @param originalLogger
	 * @param responseStatus
	 * @param logMessage
	 */

	private static void doLog(Logger originalLogger, CustomResponseStatus responseStatus, String logMessage) {
		switch (responseStatus.getLoglevel()) {
		case DEBUG:
			originalLogger.debug(logMessage);
			break;
		case INFO:
			originalLogger.info(logMessage);
			break;
		case WARN:
			originalLogger.warn(logMessage);
			break;
		case ERROR:
			originalLogger.error(logMessage);
			break;
		default:
			break;
		}
	}

}
