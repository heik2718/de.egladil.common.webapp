//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

/**
* Loglevel
*/
public enum Loglevel {

	NULL, DEBUG, INFO, WARN, ERROR;


}
