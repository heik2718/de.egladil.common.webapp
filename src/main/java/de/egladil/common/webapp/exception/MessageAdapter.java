//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import de.egladil.common.validation.json.ConstraintViolationMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.MessageLevel;

/**
 * MessageAdapter
 */
public class MessageAdapter {

	/**
	 * Adaptiert das gegebene Objekt.
	 *
	 *
	 * @param object Object
	 * @return IAdaptedMessage oder null
	 */
	public static IAdaptedMessage adapt(final Object object) {
		if (!canAdapt(object)) {
			return null;
		}
		if (object instanceof IAdaptedMessage) {
			return (IAdaptedMessage) object;
		}
		if (object instanceof ConstraintViolationMessage) {
			final ConstraintViolationMessage cvm = (ConstraintViolationMessage) object;
			return new IAdaptedMessage() {

				@Override
				public Object getPayload() {
					return cvm;
				}

				@Override
				public String getMessageLevel() {
					return MessageLevel.ERROR.toString();
				}

				@Override
				public String getMessage() {
					return cvm.getGlobalMessage();
				}
			};
		}
		if (object instanceof APIResponsePayload) {
			final APIResponsePayload pm = (APIResponsePayload) object;
			return new IAdaptedMessage() {

				@Override
				public Object getPayload() {
					return pm.getPayload();
				}

				@Override
				public String getMessageLevel() {
					return pm.getApiMessage().getLevel();
				}

				@Override
				public String getMessage() {
					return pm.getApiMessage().getMessage();
				}
			};
		}

		return null;

	}

	private static boolean canAdapt(final Object object) {
		if (object == null) {
			return false;
		}
		if (object instanceof APIResponsePayload) {
			return true;
		}
		if (object instanceof ConstraintViolationMessage) {
			return true;
		}
		if (object instanceof IAdaptedMessage) {
			return true;
		}
		return false;
	}

}
