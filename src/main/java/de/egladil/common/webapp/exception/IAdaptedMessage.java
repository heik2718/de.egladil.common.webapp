//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * IAdaptedMessage adaptiert verschiedene Message-Klassen, die in egladil-commons herumgurken.
 */
public interface IAdaptedMessage {

	@JsonProperty
	String getMessageLevel();

	@JsonProperty
	String getMessage();

	@JsonProperty
	Object getPayload();
}
