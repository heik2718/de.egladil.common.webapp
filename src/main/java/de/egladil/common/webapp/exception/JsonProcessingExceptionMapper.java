//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * JsonProcessingExceptionMapper sammelt alle Json-Exceptions auf.
 */
public class JsonProcessingExceptionMapper implements ExceptionMapper<JsonProcessingException> {

	private static final Logger LOG = LoggerFactory.getLogger(JsonProcessingExceptionMapper.class);

	/**
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(JsonProcessingException exception) {
		if (exception instanceof JsonGenerationException || exception instanceof JsonMappingException) {
			LogUtils.logError(LOG, CustomResponseStatus.SERVER_ERROR, "Fehler bei der JSON-Generierung", null, exception);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(ExceptionFactory.internalServerErrorException()).build();
		}

		final String message = exception.getMessage();
		LogUtils.logError(LOG, CustomResponseStatus.UNPROCESSABLE_ENTITY, message, null, exception);
		return Response.status(CustomResponseStatus.UNPROCESSABLE_ENTITY.getStatusCode())
			.entity(ExceptionFactory.unprocessableEntity()).build();
	}
}
