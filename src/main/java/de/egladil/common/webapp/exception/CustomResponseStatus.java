//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import javax.ws.rs.core.Response.Status;

/**
 * CustomResponseStatus ist ein Wrapper für Response.Status.
 */
public enum CustomResponseStatus {

	SERVER_ERROR(Status.INTERNAL_SERVER_ERROR, Loglevel.ERROR),
	BAD_REQUEST(Status.BAD_REQUEST, Loglevel.DEBUG),
	UNAUTHORIZED(Status.UNAUTHORIZED, Loglevel.WARN),
	NOT_FOUND(Status.NOT_FOUND, Loglevel.WARN),
	CONFLICT(Status.CONFLICT, Loglevel.DEBUG),
	UNPROCESSABLE_ENTITY(422, Loglevel.WARN),
	FORBIDDEN(Status.FORBIDDEN, Loglevel.WARN),
	REQUEST_TIMEOUT(Status.REQUEST_TIMEOUT, Loglevel.DEBUG),
	LENGTH_REQUIRED(411, Loglevel.DEBUG),
	REQUEST_ENTITY_TOO_LARGE(413, Loglevel.WARN),
	DUPLICATE_ENTRY(900, Loglevel.DEBUG),
	CONCURRENT_MODIFICATION(409, Loglevel.DEBUG),
	VALIDATION_ERRORS(902, Loglevel.DEBUG),
	MAILSERVER_NOT_AVAILABLE(903, Loglevel.DEBUG),
	NOT_SUCCESSFUL(904, Loglevel.DEBUG),
	TEAPOT(418, Loglevel.WARN),
	JWT_EXPIRED(908, Loglevel.ERROR),
	TIMEOUT(Status.REQUEST_TIMEOUT, Loglevel.DEBUG);

	private final int statusCode;

	private final Loglevel loglevel;

	private CustomResponseStatus(final int statusCode, final Loglevel loglevel) {
		this.statusCode = statusCode;
		this.loglevel = loglevel;
	}

	/**
	 * Erzeugt eine Instanz von CustomStatusCode
	 */
	private CustomResponseStatus(final Status status, final Loglevel loglevel) {
		this.statusCode = status.getStatusCode();
		this.loglevel = loglevel;
	}

	/**
	 * Liefert die Membervariable statusCode
	 *
	 * @return die Membervariable statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Liefert die Membervariable loglevel
	 *
	 * @return die Membervariable loglevel
	 */
	public Loglevel getLoglevel() {
		return loglevel;
	}

	public static CustomResponseStatus valueOfStatusCode(final int statusCode) {
		for (final CustomResponseStatus crs : CustomResponseStatus.values()) {
			if (statusCode == crs.getStatusCode()) {
				return crs;
			}
		}
		return SERVER_ERROR;
	}

}
