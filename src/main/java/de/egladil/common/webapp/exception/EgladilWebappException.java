//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * EgladilWebappException
 */
public class EgladilWebappException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	private Object payload;

	/**
	 * Erzeugt eine Instanz von EgladilWebappException
	 */
	public EgladilWebappException(final CustomResponseStatus status, final Object entity) {
		super(Response.status(status.getStatusCode()).entity(entity).type(MediaType.APPLICATION_JSON).build());
		this.payload = entity;
	}

	public final Object getPayload() {
		return payload;
	}
}
