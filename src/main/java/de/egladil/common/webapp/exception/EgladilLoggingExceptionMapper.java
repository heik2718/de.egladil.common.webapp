//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp.exception;

import java.util.Locale;
import java.util.ResourceBundle;

import io.dropwizard.jersey.errors.LoggingExceptionMapper;

/**
 * EgladilLoggingExceptionMapper
 */
public class EgladilLoggingExceptionMapper extends LoggingExceptionMapper<Throwable> {

	private final ResourceBundle applicationMessages = ResourceBundle.getBundle("ApplicationMessages", Locale.GERMAN);

	/**
	 * @see io.dropwizard.jersey.errors.LoggingExceptionMapper#formatErrorMessage(long, java.lang.Throwable)
	 */
	@Override
	protected String formatErrorMessage(long id, Throwable exception) {
		return applicationMessages.getString("general.internalServerError");
	}
}
