//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SessionToken
 */
public class SessionToken implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private String accessToken;

	@JsonProperty
	private String xsrfToken;

	/**
	 * SessionToken
	 */
	public SessionToken() {
	}

	/**
	 * SessionToken
	 */
	public SessionToken(final String accessToken, final String xsrfToken) {

		if (StringUtils.isBlank(xsrfToken)) {
			throw new IllegalArgumentException("xsrfToken blank");
		}

		this.accessToken = accessToken;
		this.xsrfToken = xsrfToken;
	}

	/**
	 * Liefert die Membervariable accessToken
	 *
	 * @return die Membervariable accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * Liefert die Membervariable xsrfToken
	 *
	 * @return die Membervariable xsrfToken
	 */
	public String getXsrfToken() {
		return xsrfToken;
	}
}
