//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.webapp;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;


/**
 * SessionTokenTest
 */
public class SessionTokenTest {

	@Test
	public void accessTokenIdDarfNullSein() {
		new SessionToken(null, "gshag");
	}

	@Test
	public void csrfTokenDarfNichtNullSein() {
		assertThrows(IllegalArgumentException.class, () -> {
			new SessionToken(null, null);
		});
	}
}
